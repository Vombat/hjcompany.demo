﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HJCompany.Demo.DataAccess.EntityFramework.ModelConfigurations
{
    class DbMatchConfiguration : IEntityTypeConfiguration<DbMatch>
    {
        public void Configure(EntityTypeBuilder<DbMatch> builder)
        {
            builder.ToTable("Matches");

        }
    }
}