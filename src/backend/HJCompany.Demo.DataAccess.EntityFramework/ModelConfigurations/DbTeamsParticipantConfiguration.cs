﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HJCompany.Demo.DataAccess.EntityFramework.ModelConfigurations
{
    public class DbTeamsParticipantConfiguration : IEntityTypeConfiguration<DbTeamsParticipant>
    {
        public void Configure(EntityTypeBuilder<DbTeamsParticipant> builder)
        {
            builder.ToTable("Participants");

            builder.HasOne(x => x.Team)
                .WithMany(x => x.Participants)
                .HasForeignKey(x => x.TeamId);

            builder.Property(x => x.FirstName)
                .HasMaxLength(Constants.FirstNameMaxLength)
                .IsRequired()
                .HasDefaultValue("");

            builder.Property(x => x.LastName)
                .HasMaxLength(Constants.LastNameMaxLength)
                .IsRequired()
                .HasDefaultValue("");
        }
    }
}