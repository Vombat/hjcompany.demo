﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HJCompany.Demo.DataAccess.EntityFramework.ModelConfigurations
{
    public class DbTeamConfiguration : IEntityTypeConfiguration<DbTeam>
    {
        public void Configure(EntityTypeBuilder<DbTeam> builder)
        {
            builder.ToTable("Teams");

            builder.HasOne(x => x.Match)
                .WithMany()
                .HasForeignKey(x => x.MatchId);

            builder.Property(x => x.Title)
                .HasMaxLength(Constants.TitleMaxLength)
                .IsRequired();

            builder.Property(x => x.Title)
                .IsRequired()
                .HasDefaultValue("");
        }
    }
}