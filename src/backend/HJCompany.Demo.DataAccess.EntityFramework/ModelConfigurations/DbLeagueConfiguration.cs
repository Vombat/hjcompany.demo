﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HJCompany.Demo.DataAccess.EntityFramework.ModelConfigurations
{
    public class DbLeagueConfiguration : IEntityTypeConfiguration<DbLeague>
    {
        public void Configure(EntityTypeBuilder<DbLeague> builder)
        {
            builder.ToTable("Leagues");

            builder.Property(x => x.Title)
                .HasMaxLength(Constants.TitleMaxLength)
                .IsRequired();

            builder.Property(x => x.Title)
                .IsRequired()
                .HasDefaultValue("");
        }
    }
}