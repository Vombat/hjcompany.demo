﻿using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using HJCompany.Demo.DataAccess.EntityFramework.Extensions;
using HJCompany.Demo.DataAccess.EntityFramework.ModelConfigurations;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace HJCompany.Demo.DataAccess.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        private readonly IEnumerable<IDbEntityPreProcessor> _preProcessors;

        public DatabaseContext(DbContextOptions<DatabaseContext> options, IEnumerable<IDbEntityPreProcessor> preProcessors)
            : base(options)
        {
            _preProcessors = preProcessors;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DbLeagueConfiguration());
            modelBuilder.ApplyConfiguration(new DbTeamsParticipantConfiguration());
            modelBuilder.ApplyConfiguration(new DbTeamConfiguration());
            modelBuilder.ApplyConfiguration(new DbMatchConfiguration());

            modelBuilder.SeedData();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entityPreProcessor in _preProcessors)
            {
                entityPreProcessor.Process(ChangeTracker);
            }

            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}