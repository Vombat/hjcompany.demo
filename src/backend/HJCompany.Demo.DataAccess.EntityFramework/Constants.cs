﻿namespace HJCompany.Demo.DataAccess.EntityFramework
{
    public static class Constants
    {        
        public const int TitleMaxLength = 2000;
        public const int FirstNameMaxLength = 100;
        public const int LastNameMaxLength = 150;        
    }
}