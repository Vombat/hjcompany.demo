﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HJCompany.Demo.DataAccess.EntityFramework.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Leagues",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "character varying(2000)", maxLength: 2000, nullable: false, defaultValue: "")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leagues", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Matches",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    LeagueId = table.Column<Guid>(type: "uuid", nullable: false),
                    Date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    MatchId = table.Column<Guid>(type: "uuid", nullable: true),
                    Title = table.Column<string>(type: "character varying(2000)", maxLength: 2000, nullable: false, defaultValue: ""),
                    DbMatchId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teams_Matches_DbMatchId",
                        column: x => x.DbMatchId,
                        principalTable: "Matches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teams_Matches_MatchId",
                        column: x => x.MatchId,
                        principalTable: "Matches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Participants",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    TeamId = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false, defaultValue: ""),
                    LastName = table.Column<string>(type: "character varying(150)", maxLength: 150, nullable: false, defaultValue: "")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Participants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Participants_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Leagues",
                columns: new[] { "Id", "Title" },
                values: new object[] { new Guid("88d3602d-3664-44a3-9c02-4600c8d59ace"), "Лига №01" });

            migrationBuilder.InsertData(
                table: "Matches",
                columns: new[] { "Id", "Date", "LeagueId" },
                values: new object[] { new Guid("59873d6f-4ae7-4694-8511-7c44ed1bdff0"), new DateTime(2021, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("88d3602d-3664-44a3-9c02-4600c8d59ace") });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "DbMatchId", "MatchId", "Title" },
                values: new object[,]
                {
                    { new Guid("bd5b2656-1e82-4f44-9ea3-cd0f10e85f82"), null, new Guid("59873d6f-4ae7-4694-8511-7c44ed1bdff0"), "Команда 01" },
                    { new Guid("43af38b2-7a02-428c-9d85-5605ade6580e"), null, new Guid("59873d6f-4ae7-4694-8511-7c44ed1bdff0"), "Команда 02" }
                });

            migrationBuilder.InsertData(
                table: "Participants",
                columns: new[] { "Id", "FirstName", "LastName", "TeamId" },
                values: new object[,]
                {
                    { new Guid("5a8e1b9e-8777-4513-aaea-e005f3325f64"), "Имя-0101", "Фамилия-0101", new Guid("bd5b2656-1e82-4f44-9ea3-cd0f10e85f82") },
                    { new Guid("909286b3-d3db-43c9-afd6-7c2aa20599c5"), "Имя-0102", "Фамилия-0102", new Guid("bd5b2656-1e82-4f44-9ea3-cd0f10e85f82") },
                    { new Guid("d52fc044-c422-4f31-9e51-47d52b60042b"), "Имя-0201", "Фамилия-0201", new Guid("43af38b2-7a02-428c-9d85-5605ade6580e") },
                    { new Guid("acb5de38-a0b7-481e-bcfc-52bbed7f27ea"), "Имя-0201", "Фамилия-0201", new Guid("43af38b2-7a02-428c-9d85-5605ade6580e") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Participants_TeamId",
                table: "Participants",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Teams_DbMatchId",
                table: "Teams",
                column: "DbMatchId");

            migrationBuilder.CreateIndex(
                name: "IX_Teams_MatchId",
                table: "Teams",
                column: "MatchId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Leagues");

            migrationBuilder.DropTable(
                name: "Participants");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "Matches");
        }
    }
}
