﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using HJCompany.Demo.DataAccess.EntityFramework.Seeds;
using Microsoft.EntityFrameworkCore;

namespace HJCompany.Demo.DataAccess.EntityFramework.Extensions
{
    internal static class ModelBuilderExtensions
    {
        public static void SeedData(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbLeague>().HasData(StaticSeedData.League.Create());

            modelBuilder.Entity<DbMatch>().HasData(StaticSeedData.Match.Create());

            modelBuilder.Entity<DbTeam>().HasData(
                StaticSeedData.Team01.Create(),
                StaticSeedData.Team02.Create()
                );


            modelBuilder.Entity<DbTeamsParticipant>().HasData(
                StaticSeedData.TeamsParticipant0101.Create(),
                StaticSeedData.TeamsParticipant0102.Create(),
                StaticSeedData.TeamsParticipant0201.Create(),
                StaticSeedData.TeamsParticipant0202.Create()
            );
        }
    }
}