﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using System;

namespace HJCompany.Demo.DataAccess.EntityFramework.Seeds
{
    internal class LeagueSeed : DbLeague
    {        
        public LeagueSeed(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public DbLeague Create()
        {
            return new DbLeague(Title) { Id = Id};
        }
    }
}