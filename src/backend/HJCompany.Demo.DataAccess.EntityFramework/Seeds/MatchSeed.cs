﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using System;

namespace HJCompany.Demo.DataAccess.EntityFramework.Seeds
{
    internal class MatchSeed : DbMatch
    {
        public MatchSeed(Guid id, Guid leagueId, DateTime date)
        {
            Id = id;
            LeagueId = leagueId;
            Date = date;
        }

        public DbMatch Create()
        {
            return new DbMatch(LeagueId, Date) { Id = Id };
        }
    }
}