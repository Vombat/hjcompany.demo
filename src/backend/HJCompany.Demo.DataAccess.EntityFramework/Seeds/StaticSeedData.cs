﻿using System;

namespace HJCompany.Demo.DataAccess.EntityFramework.Seeds
{
    public static class StaticSeedData
    {
        private static Guid LeagueId = Guid.Parse("88D3602D-3664-44A3-9C02-4600C8D59ACE");
        private static Guid MatchId = Guid.Parse("59873D6F-4AE7-4694-8511-7C44ED1BDFF0");
        private static Guid TeamId01 = Guid.Parse("BD5B2656-1E82-4F44-9EA3-CD0F10E85F82");
        private static Guid TeamId02 = Guid.Parse("43AF38B2-7A02-428C-9D85-5605ADE6580E");
        private static readonly DateTime Date = new DateTime(2021, 2, 1);

        internal static LeagueSeed League => new LeagueSeed(LeagueId, "Лига №01");
        internal static MatchSeed Match => new MatchSeed(MatchId, LeagueId, Date);
        internal static TeamSeed Team01 => new TeamSeed(TeamId01, MatchId, "Команда 01");
        internal static TeamSeed Team02 => new TeamSeed(TeamId02, MatchId, "Команда 02");

        internal static TeamsParticipantSeed TeamsParticipant0101 => new TeamsParticipantSeed(Guid.Parse("5A8E1B9E-8777-4513-AAEA-E005F3325F64"), TeamId01, "Имя-0101", "Фамилия-0101");
        internal static TeamsParticipantSeed TeamsParticipant0102 => new TeamsParticipantSeed(Guid.Parse("909286B3-D3DB-43C9-AFD6-7C2AA20599C5"), TeamId01, "Имя-0102", "Фамилия-0102");

        internal static TeamsParticipantSeed TeamsParticipant0201 => new TeamsParticipantSeed(Guid.Parse("D52FC044-C422-4F31-9E51-47D52B60042B"), TeamId02, "Имя-0201", "Фамилия-0201");
        internal static TeamsParticipantSeed TeamsParticipant0202 => new TeamsParticipantSeed(Guid.Parse("ACB5DE38-A0B7-481E-BCFC-52BBED7F27EA"), TeamId02, "Имя-0201", "Фамилия-0201");
    }
}