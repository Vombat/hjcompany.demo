﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using System;

namespace HJCompany.Demo.DataAccess.EntityFramework.Seeds
{
    internal class TeamSeed : DbTeam
    {
        public TeamSeed(Guid id, Guid? matchId, string title)
        {
            Id = id;
            MatchId = matchId;
            Title = title;
        }

        public DbTeam Create()
        {
            return new DbTeam(MatchId, Title) { Id = Id};
        }
    }
}