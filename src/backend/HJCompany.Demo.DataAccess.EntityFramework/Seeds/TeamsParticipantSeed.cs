﻿using HJCompany.Demo.DataAccess.Contracts.Models;
using System;

namespace HJCompany.Demo.DataAccess.EntityFramework.Seeds
{
    internal class TeamsParticipantSeed : DbTeamsParticipant
    {
        public TeamsParticipantSeed(Guid id, Guid teamId, string firstName, string lastName)
        {
            Id = id;
            TeamId = teamId;
            FirstName = firstName;
            LastName = lastName;
        }

        public DbTeamsParticipant Create()
        {
            return new DbTeamsParticipant(TeamId, FirstName, LastName) { Id = Id};
        }
    }
}