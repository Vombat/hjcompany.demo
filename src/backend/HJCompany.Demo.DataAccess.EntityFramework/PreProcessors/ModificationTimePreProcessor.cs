﻿using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;

namespace HJCompany.Demo.DataAccess.EntityFramework.PreProcessors
{
    public class ModificationTimePreProcessor : IDbEntityPreProcessor
    {
        public void Process(ChangeTracker changeTracker)
        {
            foreach (var entry in changeTracker.Entries<ICreationDateTrackedEntity>()
                .Where(x => x.State == EntityState.Added).Where(x => x.Entity != null))
            {
                entry.Entity.DateCreated = DateTimeOffset.UtcNow;
            }
        }
    }
}