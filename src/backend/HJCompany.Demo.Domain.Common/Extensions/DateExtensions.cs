﻿using System;

namespace HJCompany.Demo.Domain.Common.Extensions
{
    public static class DateExtensions
    {
        public static DateTime? ToDate(this int? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            var value = date.Value;
            return new DateTime(value / 10000, value % 10000 / 100, value % 100);
        }

        public static int? ToDateInt(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }

            var value = date.Value;
            return value.Year * 10000 + value.Month * 100 + value.Day;
        }
    }
}