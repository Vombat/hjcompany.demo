﻿using System;

namespace HJCompany.Demo.Domain.Contracts.Models.Team
{
    public class UpdateTeamsParticipantModel
    {
        protected UpdateTeamsParticipantModel() { }

        public UpdateTeamsParticipantModel(Guid teamId, string firstName, string lastName)
        {
            TeamId = teamId;
            FirstName = firstName;
            LastName = lastName;
        }

        public Guid TeamId { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
    }
}