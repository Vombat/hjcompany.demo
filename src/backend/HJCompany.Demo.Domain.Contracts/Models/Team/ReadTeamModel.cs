﻿using System;
using System.Collections.Generic;

namespace HJCompany.Demo.Domain.Contracts.Models.Team
{
    public class ReadTeamModel
    {
        protected ReadTeamModel() { }

        public ReadTeamModel(Guid id, Guid? matchId, string title, IEnumerable<ReadTeamsParticipantModel> participants)
        {
            Id = id;
            MatchId = matchId;
            Title = title;
            Participants = participants;
        }

        public Guid Id { get; protected set; }
        public Guid? MatchId { get; set; }
        public string Title { get; protected set; }
        public IEnumerable<ReadTeamsParticipantModel> Participants { get; protected set; }
    }
}