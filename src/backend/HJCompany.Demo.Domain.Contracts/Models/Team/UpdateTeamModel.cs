﻿using System;
using System.Collections.Generic;

namespace HJCompany.Demo.Domain.Contracts.Models.Team
{
    public class UpdateTeamModel
    {
        protected UpdateTeamModel() { }

        public UpdateTeamModel(Guid? matchId, string title, IEnumerable<ReadTeamsParticipantModel> participants)
        {
            MatchId = matchId;
            Title = title;
            Participants = participants;
        }

        public Guid? MatchId;
        public string Title { get; protected set; }
        public IEnumerable<ReadTeamsParticipantModel> Participants { get; protected set; }
    }
}