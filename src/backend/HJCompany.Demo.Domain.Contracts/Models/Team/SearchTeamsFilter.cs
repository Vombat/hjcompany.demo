﻿namespace HJCompany.Demo.Domain.Contracts.Models.Team
{
    public class SearchTeamsFilter
    {
        protected SearchTeamsFilter() { }

        public SearchTeamsFilter(string textFilter, int offset, int limit)
        {
            TextFilter = textFilter;
            Offset = offset;
            Limit = limit;
        }

        public string TextFilter { get; protected set; }
        public int Offset { get; protected set; }
        public int Limit { get; protected set; }
    }
}