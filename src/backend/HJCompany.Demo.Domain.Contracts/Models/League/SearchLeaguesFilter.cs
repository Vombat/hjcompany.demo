﻿namespace HJCompany.Demo.Domain.Contracts.Models.League
{
    public class SearchLeaguesFilter
    {
        protected SearchLeaguesFilter() { }

        public SearchLeaguesFilter(string textFilter, int offset, int limit)
        {
            TextFilter = textFilter;
            Offset = offset;
            Limit = limit;
        }

        public string TextFilter { get; protected set; }
        public int Offset { get; protected set; }
        public int Limit { get; protected set; }
    }
}