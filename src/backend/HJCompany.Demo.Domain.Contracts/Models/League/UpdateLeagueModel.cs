﻿namespace HJCompany.Demo.Domain.Contracts.Models.League
{
    public class UpdateLeagueModel
    {
        protected UpdateLeagueModel() { }

        public UpdateLeagueModel(string title)
        {
            Title = title;
        }

        public string Title { get; protected set; }
    }
}
