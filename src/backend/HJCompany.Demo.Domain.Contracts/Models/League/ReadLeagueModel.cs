﻿using System;

namespace HJCompany.Demo.Domain.Contracts.Models.League
{
    public class ReadLeagueModel
    {
        protected ReadLeagueModel() { }

        public ReadLeagueModel(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; protected set; }
        public string Title { get; protected set; }
    }
}