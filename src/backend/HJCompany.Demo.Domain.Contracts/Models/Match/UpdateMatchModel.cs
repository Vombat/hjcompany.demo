﻿using HJCompany.Demo.Domain.Contracts.Models.Team;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.Domain.Contracts.Models.Match
{
    public class UpdateMatchModel
    {
        protected UpdateMatchModel() { }

        public UpdateMatchModel(Guid leagueId, DateTime date, IEnumerable<ReadTeamModel> teams)
        {
            LeagueId = leagueId;
            Date = date;
            Teams = teams;
        }

        public Guid LeagueId { get; protected set; }
        public DateTime? Date { get; protected set; }
        public IEnumerable<ReadTeamModel> Teams { get; protected set; }
    }
}