﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HJCompany.Demo.Domain.Contracts.Models.Match
{
    public class SearchMatchesFilter
    {
        protected SearchMatchesFilter() { }

        public SearchMatchesFilter(Guid? leagueId, DateTime? date,int offset, int limit)
        {
            LeagueId = leagueId;
            Date = date;
            Offset = offset;
            Limit = limit;
        }

        public Guid? LeagueId { get; protected set; }
        public DateTime? Date { get; protected set; }        
        public int Offset { get; protected set; }
        public int Limit { get; protected set; }
    }
}
