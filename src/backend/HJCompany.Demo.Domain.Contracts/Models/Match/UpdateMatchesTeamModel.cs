﻿using HJCompany.Demo.Domain.Contracts.Models.Team;

namespace HJCompany.Demo.Domain.Contracts.Models.Match
{
    public class UpdateMatchesTeamModel
    {
        protected UpdateMatchesTeamModel() { }

        public UpdateMatchesTeamModel(UpdateTeamModel matchesTeam)
        {
            MatchesTeam = matchesTeam;
        }

        public UpdateTeamModel MatchesTeam { get; set; }
    }
}