﻿using HJCompany.Demo.Domain.Contracts.Models.Team;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.Domain.Contracts.Models.Match
{
    public class ReadMatchModel
    {
        protected ReadMatchModel() { }

        public ReadMatchModel(Guid id, Guid leagueId, DateTime date, IEnumerable<ReadTeamModel> teams)
        {
            Id = id;
            LeagueId = leagueId;
            Date = date;
            Teams = teams;
        }

        public Guid Id { get; protected set; }
        public Guid LeagueId { get; protected set; }
        public DateTime Date { get; protected set; }
        public IEnumerable<ReadTeamModel> Teams { get; protected set; }
    }
}