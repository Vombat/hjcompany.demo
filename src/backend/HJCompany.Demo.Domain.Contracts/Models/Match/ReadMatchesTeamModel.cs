﻿using HJCompany.Demo.Domain.Contracts.Models.Team;

namespace HJCompany.Demo.Domain.Contracts.Models.Match
{
    public class ReadMatchesTeamModel
    {
        protected ReadMatchesTeamModel() { }

        public ReadMatchesTeamModel(ReadTeamModel matchesTeam)
        {
            MatchesTeam = matchesTeam;
        }

        public ReadTeamModel MatchesTeam { get; protected set; }
    }
}