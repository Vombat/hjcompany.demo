﻿using System.Collections.Generic;

namespace HJCompany.Demo.Domain.Contracts.Models
{
    public class PaginationReadModel<T> where T : class
    {
        protected PaginationReadModel() { }

        public PaginationReadModel(IEnumerable<T> items, int count)
        {
            Items = items;
            Count = count;
        }

        public IEnumerable<T> Items { get; protected set; } = new List<T>();

        public int Count { get; protected set; }
    }
}