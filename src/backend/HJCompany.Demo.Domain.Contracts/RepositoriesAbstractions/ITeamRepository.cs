﻿using HJCompany.Demo.Domain.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.Team;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions
{
    public interface ITeamRepository
    {
        Task<ReadTeamModel> GetTeamAsync(Guid id, CancellationToken cancellationToken);
        Task<PaginationReadModel<ReadTeamModel>> SearchTeamsAsync(SearchTeamsFilter filter, CancellationToken cancellationToken);
        Task<ReadTeamModel> CreateTeamAsync(CreateTeamModel model, CancellationToken cancellationToken);
        Task<ReadTeamModel> UpdateTeamAsync(Guid id, UpdateTeamModel model, CancellationToken cancellationToken);
        Task DeleteTeamAsync(Guid id, CancellationToken cancellationToken);
        Task<ReadTeamsParticipantModel> AddTeamsParticipantAsync(Guid teamId, CreateTeamsParticipantModel model, CancellationToken cancellationToken);
        Task<ReadTeamsParticipantModel> UpdateTeamsParticipantAsync(Guid teamId, Guid participantId, UpdateTeamsParticipantModel model, CancellationToken cancellationToken);
        Task DeleteTeamsParticipantAsync(Guid teamId, Guid participantId, CancellationToken cancellationToken);
    }
}