﻿using HJCompany.Demo.Domain.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.Match;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions
{
    public interface IMatchRepository
    {
        Task<ReadMatchModel> GetMatchAsync(Guid id, CancellationToken cancellationToken);
        Task<PaginationReadModel<ReadMatchModel>> SearchMatchesAsync(SearchMatchesFilter filter, CancellationToken cancellationToken);
        Task<ReadMatchModel> CreateMatchAsync(CreateMatchModel model, CancellationToken cancellationToken);
        Task<ReadMatchModel> UpdateMatchAsync(Guid id, UpdateMatchModel model, CancellationToken cancellationToken);
        Task DeleteMatchAsync(Guid id, CancellationToken cancellationToken);
        Task<ReadMatchesTeamModel> AddMatchesTeamAsync(Guid matchId, CreateMatchesTeamModel model, CancellationToken cancellationToken);
        Task<ReadMatchesTeamModel> UpdateMatchesTeamAsync(Guid matchId, Guid teamId, UpdateMatchesTeamModel model, CancellationToken cancellationToken);
        Task DeleteMatchesTeamAsync(Guid matchId, Guid teamId, CancellationToken cancellationToken);
    }
}