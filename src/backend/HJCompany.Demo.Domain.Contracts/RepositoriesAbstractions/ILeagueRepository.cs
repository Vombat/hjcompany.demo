﻿using HJCompany.Demo.Domain.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.League;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions
{
    public interface ILeagueRepository
    {
        Task<ReadLeagueModel> GetLeagueAsync(Guid id, CancellationToken cancellationToken);
        Task<PaginationReadModel<ReadLeagueModel>> SearchLeaguesAsync(SearchLeaguesFilter filter, CancellationToken cancellationToken);
        Task<ReadLeagueModel> CreateLeagueAsync(CreateLeagueModel model, CancellationToken cancellationToken);
        Task<ReadLeagueModel> UpdateLeagueAsync(Guid id, UpdateLeagueModel model, CancellationToken cancellationToken);
        Task DeleteLeagueAsync(Guid id, CancellationToken cancellationToken);
    }
}