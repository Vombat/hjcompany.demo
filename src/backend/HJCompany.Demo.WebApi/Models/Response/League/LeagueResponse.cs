﻿using System;

namespace HJCompany.Demo.WebApi.Models.Response.League
{
    public class LeagueResponse
    {
        protected LeagueResponse() { }

        public LeagueResponse(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; protected set; }
        public string Title { get; protected set; }
    }
}