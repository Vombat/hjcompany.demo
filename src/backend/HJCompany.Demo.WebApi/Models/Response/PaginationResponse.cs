﻿using System.Collections.Generic;

namespace HJCompany.Demo.WebApi.Models.Response
{
    public class PaginationResponse<T> where T : class
    {
        protected PaginationResponse() { }

        public PaginationResponse(IEnumerable<T> items, int count)
        {
            Items = items;
            Count = count;
        }

        public IEnumerable<T> Items { get; protected set; }

        public int Count { get; protected set; }
    }
}