﻿using System;

namespace HJCompany.Demo.WebApi.Models.Response.Team
{
    public class TeamsParticipantResponse
    {
        protected TeamsParticipantResponse() { }

        public TeamsParticipantResponse(Guid id, Guid teamId, string firstName, string lastName)
        {
            Id = id;
            TeamId = teamId;
            FirstName = firstName;
            LastName = lastName;
        }

        public Guid Id { get; protected set; }
        public Guid TeamId { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
    }
}