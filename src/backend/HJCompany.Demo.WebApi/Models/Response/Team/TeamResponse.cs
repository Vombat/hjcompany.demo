﻿using System;
using System.Collections.Generic;

namespace HJCompany.Demo.WebApi.Models.Response.Team
{
    public class TeamResponse
    {
        protected TeamResponse() { }

        public TeamResponse(Guid id, Guid? matchId, string title, IEnumerable<TeamsParticipantResponse> participants)
        {
            Id = id;
            MatchId = matchId;
            Title = title;
            Participants = participants;
        }

        public Guid Id { get; protected set; }
        public Guid? MatchId { get; set; }
        public string Title { get; protected set; }
        public IEnumerable<TeamsParticipantResponse> Participants { get; protected set; }
    }
}