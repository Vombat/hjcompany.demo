﻿using HJCompany.Demo.WebApi.Models.Response.Team;

namespace HJCompany.Demo.WebApi.Models.Response.Match
{
    public class MatchesTeamResponse
    {
        protected MatchesTeamResponse() { }

        public MatchesTeamResponse(TeamResponse matchesTeam)
        {
            MatchesTeam = matchesTeam;
        }

        public TeamResponse MatchesTeam { get; protected set; }
    }
}