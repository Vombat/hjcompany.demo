﻿using HJCompany.Demo.WebApi.Models.Response.Team;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.WebApi.Models.Response.Match
{
    public class MatchResponse
    {
        protected MatchResponse() { }

        public MatchResponse(Guid id, Guid leagueId, DateTime date, IEnumerable<TeamResponse> teams)
        {
            Id = id;
            LeagueId = leagueId;
            Date = date;
            Teams = teams;
        }

        public Guid Id { get; protected set; }
        public Guid LeagueId { get; protected set; }
        public DateTime Date { get; protected set; }
        public IEnumerable<TeamResponse> Teams { get; protected set; }
    }
}