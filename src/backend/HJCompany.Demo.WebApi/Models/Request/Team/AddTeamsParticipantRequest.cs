﻿namespace HJCompany.Demo.WebApi.Models.Request.Team
{
    public class AddTeamsParticipantRequest : CreateTeamsParticipantRequest { }

    public class AddTeamsParticipantRequestValidator : CreateTeamsParticipantRequestValidator<AddTeamsParticipantRequest> { }
}