﻿using FluentValidation;
using HJCompany.Demo.WebApi.Infrastructure;

namespace HJCompany.Demo.WebApi.Models.Request.Team
{
    /// <summary>
    ///     Создание участника команды
    /// </summary>
    public class CreateTeamsParticipantRequest
    {
        /// <summary>
        ///     Имя участника команды
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     Фамилия участника команды
        /// </summary>
        public string LastName { get; set; }
    }

    public class CreateTeamsParticipantRequestValidator<T> : AbstractValidator<T> where T : CreateTeamsParticipantRequest
    {
        public CreateTeamsParticipantRequestValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage(@"First name must have a non-empty description")
                .MaximumLength(Constants.FirstNameMaxLength)
                .WithMessage($@"First name must no more than {Constants.FirstNameMaxLength} characters");

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage(@"Last name must have a non-empty description")
                .MaximumLength(Constants.LastNameMaxLength)
                .WithMessage($@"Last name must no more than {Constants.LastNameMaxLength} characters");
        }
    }
}