﻿using FluentValidation;
using System;

namespace HJCompany.Demo.WebApi.Models.Request.Team
{
    /// <summary>
    ///     Получение команды по идентификатору
    /// </summary>
    public class ByTeamIdAndParticipantRequest
    {        
        public Guid TeamId { get; set; }
        public Guid ParticipantId { get; set; }
    }

    public class ByTeamIdAndParticipantRequestValidator : AbstractValidator<ByTeamIdAndParticipantRequest>
    {
        public ByTeamIdAndParticipantRequestValidator()
        {
            RuleFor(x => x.TeamId)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"TeamId should be specified and non-empty GUID\UUID"); 
            
            RuleFor(x => x.ParticipantId)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"TeamId should be specified and non-empty GUID\UUID");
        }
    }
}