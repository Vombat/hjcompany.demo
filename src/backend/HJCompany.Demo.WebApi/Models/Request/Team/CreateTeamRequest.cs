﻿using FluentValidation;
using HJCompany.Demo.WebApi.Infrastructure;
using System.Collections.Generic;

namespace HJCompany.Demo.WebApi.Models.Request.Team
{
    /// <summary>
    ///     Создание команды
    /// </summary>
    public class CreateTeamRequest
    {
        /// <summary>
        ///     Наименование команды
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Участники команды
        /// </summary>
        public IEnumerable<CreateTeamsParticipantRequest> Participants { get; set; }
    }

    public class CreateTeamRequestValidator : AbstractValidator<CreateTeamRequest>
    {
        public CreateTeamRequestValidator()
        {
            RuleFor(x => x.Title)
                .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.TitleTeamMaxLength)
                .WithMessage($"Team must have a non-empty title no more than {Constants.TitleTeamMaxLength} characters");

            RuleFor(x => x.Participants)
                .NotEmpty()
                .WithMessage(@"There must be at least one participant item");

            RuleForEach(x => x.Participants)
                .SetValidator(new CreateTeamsParticipantRequestValidator<CreateTeamsParticipantRequest>());
        }
    }
}