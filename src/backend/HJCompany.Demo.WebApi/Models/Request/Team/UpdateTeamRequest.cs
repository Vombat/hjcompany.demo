﻿using FluentValidation;
using HJCompany.Demo.WebApi.Infrastructure;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.WebApi.Models.Request.Team
{
    /// <summary>
    ///     Обновление команды
    /// </summary>
    public class UpdateTeamRequest
    {
        /// <summary>
        ///     Матч
        /// </summary>
        public Guid? MatchId { get; set; }

        /// <summary>
        ///     Новое наименование команды
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     Участники команды
        /// </summary>
        public IEnumerable<UpdateTeamsParticipantRequest> Participants { get; set; }
    }

    public class UpdateTeamRequestValidator : AbstractValidator<UpdateTeamRequest>
    {
        public UpdateTeamRequestValidator()
        {
            When(x => x.MatchId.HasValue, () =>
            {
                RuleFor(x => x.MatchId.Value)
                    .Must(x => x != Guid.Empty && x != default)
                    .WithMessage(@"MatchId should be specified and non-empty GUID\UUID");
            });

            RuleFor(x => x.Title)
                .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.TitleTeamMaxLength)
                .WithMessage($"Team must have a non-empty title no more than {Constants.TitleTeamMaxLength} characters");

            RuleFor(x => x.Participants)
                .NotEmpty()
                .WithMessage(@"There must be at least one participant item");

            RuleForEach(x => x.Participants)
                .SetValidator(new UpdateTeamsParticipantRequestValidator());
        }
    }
}