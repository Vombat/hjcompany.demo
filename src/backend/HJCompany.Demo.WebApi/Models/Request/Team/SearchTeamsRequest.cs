﻿using FluentValidation;
using HJCompany.Demo.WebApi.Infrastructure;

namespace HJCompany.Demo.WebApi.Models.Request.Team
{
    /// <summary>
    ///     Поиск команды по фильтру
    /// </summary>
    public class SearchTeamsRequest : LimitOffsetRequest
    {
        /// <summary>
        ///     Текстовый фильтр
        /// </summary>
        public string TextFilter { get; set; }
    }

    public class SearchTeamRequestValidator : LimitOffsetRequestValidator<SearchTeamsRequest>
    {
        public SearchTeamRequestValidator()
        {
            When(x => !string.IsNullOrWhiteSpace(x.TextFilter), () =>
            {
                RuleFor(x => x.TextFilter)
                    .MinimumLength(Constants.TextFilterMinLength)
                    .WithMessage($@"When specified, TextFilter should be minimum of {Constants.TextFilterMinLength} characters");
            });
        }
    }
}