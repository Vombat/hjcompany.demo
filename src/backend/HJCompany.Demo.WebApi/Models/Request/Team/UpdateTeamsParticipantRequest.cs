﻿namespace HJCompany.Demo.WebApi.Models.Request.Team
{
    public class UpdateTeamsParticipantRequest : CreateTeamsParticipantRequest { }

    public class UpdateTeamsParticipantRequestValidator : CreateTeamsParticipantRequestValidator <UpdateTeamsParticipantRequest>{ }
}