﻿using FluentValidation;
using HJCompany.Demo.WebApi.Infrastructure;

namespace HJCompany.Demo.WebApi.Models.Request
{
    public abstract class LimitOffsetRequest
    {
        /// <summary>
        ///     Лимит выборки
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        ///     Отступ для выборки
        /// </summary>
        public int Offset { get; set; }
    }

    public class LimitOffsetRequestValidator<T> : AbstractValidator<T> where T : LimitOffsetRequest
    {
        public LimitOffsetRequestValidator()
        {
            RuleFor(x => x.Limit)
                .GreaterThan(Constants.MinLimit)
                .WithMessage($"Limit should be greater than {Constants.MinLimit}");

            RuleFor(x => x.Offset)
                .GreaterThanOrEqualTo(Constants.MinOffset)
                .WithMessage($"Offset should be greater or equal to {Constants.MinOffset}");
        }
    }
}