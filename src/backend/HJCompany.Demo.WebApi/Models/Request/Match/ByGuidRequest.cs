﻿using FluentValidation;
using System;

namespace HJCompany.Demo.WebApi.Models.Request.Match
{
    /// <summary>
    ///     Получение матча по идентификатору
    /// </summary>
    public class ByMatchIdAndTeamRequest
    {       
        public Guid MatchId { get; set; }
        public Guid TeamId { get; set; }
    }

    public class ByMatchIdAndTeamRequestValidator : AbstractValidator<ByMatchIdAndTeamRequest>
    {
        public ByMatchIdAndTeamRequestValidator()
        {
            RuleFor(x => x.MatchId)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"MatchId should be specified and non-empty GUID\UUID");
            
            RuleFor(x => x.TeamId)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"MatchId should be specified and non-empty GUID\UUID");
        }
    }
}