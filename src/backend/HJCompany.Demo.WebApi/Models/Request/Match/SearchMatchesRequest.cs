﻿using FluentValidation;
using HJCompany.Demo.Domain.Common.Extensions;
using System;

namespace HJCompany.Demo.WebApi.Models.Request.Match
{
    /// <summary>
    ///     Поиск матча по фильтру
    /// </summary>
    public class SearchMatchesRequest : LimitOffsetRequest
    {
        /// <summary>
        ///     Поиск матча по идентификатору лиги
        /// </summary>
        public Guid? LeagueId { get; set; }

        /// <summary>
        ///     Поиск команды по дате проведения
        /// </summary>
        public int? Date { get; set; }
    }

    public class SearchMatchRequestValidator : LimitOffsetRequestValidator<SearchMatchesRequest>
    {
        public SearchMatchRequestValidator()
        {
            When(x => x.LeagueId.HasValue, () =>
            {
                RuleFor(x => x.LeagueId.Value)
                    .Must(x => x != Guid.Empty && x != default)
                    .WithMessage(@"MatchId should be specified and non-empty GUID\UUID");
            });

            When(x => x.Date.HasValue, () =>
            {
                RuleFor(x => x.Date.ToDate())
                    .NotNull()
                    .WithMessage("The date of the match must not be empty");
            });
        }
    }
}