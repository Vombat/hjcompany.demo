﻿using FluentValidation;
using HJCompany.Demo.Domain.Common.Extensions;
using HJCompany.Demo.WebApi.Models.Request.Team;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.WebApi.Models.Request.Match
{
    /// <summary>
    ///     Обновление матча
    /// </summary>
    public class UpdateMatchRequest
    {
        /// <summary>
        ///     Лига матча
        /// </summary>
        public Guid? LeagueId { get; set; }

        /// <summary>
        ///     Дата проведения матча
        /// </summary>
        public int? Date { get; set; }

        /// <summary>
        ///     Команды заявленные на матч
        /// </summary>
        public IEnumerable<UpdateTeamRequest> Teams { get; set; }
    }

    public class UpdateMatchRequestValidator : AbstractValidator<UpdateMatchRequest>
    {
        public UpdateMatchRequestValidator()
        {
            When(x => x.LeagueId.HasValue, () =>
            {
                RuleFor(x => x.LeagueId.Value)
                    .Must(x => x != Guid.Empty && x != default)
                    .WithMessage(@"MatchId should be specified and non-empty GUID\UUID");
            });

            When(x => x.Date.HasValue, () =>
            {
                RuleFor(x => x.Date.ToDate())
                    .NotNull()
                    .WithMessage("The date of the match must not be empty");
            });

            RuleFor(x => x.Teams)
                .NotEmpty()
                .WithMessage(@"There must be at least one team");

            RuleForEach(x => x.Teams)
                .SetValidator(new UpdateTeamRequestValidator());
        }
    }
}