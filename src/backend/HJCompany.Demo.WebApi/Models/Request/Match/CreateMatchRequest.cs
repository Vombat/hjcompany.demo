﻿using FluentValidation;
using HJCompany.Demo.Domain.Common.Extensions;
using HJCompany.Demo.WebApi.Models.Request.Team;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.WebApi.Models.Request.Match
{
    /// <summary>
    ///     Создание матча
    /// </summary>
    public class CreateMatchRequest
    {
        /// <summary>
        ///     Лига матча
        /// </summary>
        public Guid LeagueId { get; set; }

        /// <summary>
        ///     Дата проведения матча
        /// </summary>
        public int? Date { get; set; }

        /// <summary>
        ///     Команды заявленные на матч
        /// </summary>
        public IEnumerable<CreateTeamRequest> Teams { get; set; }
    }

    public class CreateMatchRequestValidator : AbstractValidator<CreateMatchRequest>
    {
        public CreateMatchRequestValidator()
        {
            RuleFor(x => x.LeagueId)
                    .Must(x => x != Guid.Empty && x != default)
                    .WithMessage(@"LeagueId should be specified and non-empty GUID\UUID");

            RuleFor(x => x.Date.ToDate())
                     .NotNull()
                     .WithMessage("The date of the match must not be empty");

            RuleFor(x => x.Teams)
                .NotEmpty()
                .WithMessage(@"There must be at least one team");

            RuleForEach(x => x.Teams)
                .SetValidator(new CreateTeamRequestValidator());
        }
    }
}