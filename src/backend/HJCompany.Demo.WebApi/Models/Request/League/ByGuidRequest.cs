﻿using FluentValidation;
using System;

namespace HJCompany.Demo.WebApi.Models.Request.League
{
    /// <summary>
    ///     Получение лиги по идентификатору
    /// </summary>
    public class ByLeagueIdRequest
    {
        /// <summary>
        ///     Идентификатор лиги
        /// </summary>
        public Guid Id { get; set; }
    }

    public class ByLeagueIdRequestValidator : AbstractValidator<ByLeagueIdRequest>
    {
        public ByLeagueIdRequestValidator()
        {
            RuleFor(x => x.Id)
                .Must(x => x != Guid.Empty && x != default)
                .WithMessage(@"LeagueId should be specified and non-empty GUID\UUID");
        }
    }
}