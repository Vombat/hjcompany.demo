﻿namespace HJCompany.Demo.WebApi.Models.Request.League
{
    public class UpdateLeagueRequest : CreateLeagueRequest { }

    public class UpdateLeagueRequestValidator : CreateLeagueRequestValidator { }
}