﻿using FluentValidation;
using HJCompany.Demo.WebApi.Infrastructure;

namespace HJCompany.Demo.WebApi.Models.Request.League
{
    /// <summary>
    ///     Поиск лиги по фильтру
    /// </summary>
    public class SearchLeaguesRequest : LimitOffsetRequest
    {
        /// <summary>
        ///     Текстовый фильтр
        /// </summary>
        public string TextFilter { get; set; }
    }

    public class SearchLeagueRequestValidator : LimitOffsetRequestValidator<SearchLeaguesRequest>
    {
        public SearchLeagueRequestValidator()
        {
            When(x => !string.IsNullOrWhiteSpace(x.TextFilter), () =>
            {
                RuleFor(x => x.TextFilter)
                    .MinimumLength(Constants.TextFilterMinLength)
                    .WithMessage($@"When specified, TextFilter should be minimum of {Constants.TextFilterMinLength} characters");
            });
        }
    }
}