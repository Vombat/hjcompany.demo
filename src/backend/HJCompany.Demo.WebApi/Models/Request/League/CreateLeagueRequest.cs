﻿using FluentValidation;
using HJCompany.Demo.WebApi.Infrastructure;

namespace HJCompany.Demo.WebApi.Models.Request.League
{
    /// <summary>
    ///     Создание лиги
    /// </summary>
    public class CreateLeagueRequest
    {
        /// <summary>
        ///     Наименование лиги
        /// </summary>
        public string Title { get; set; }
    }

    public class CreateLeagueRequestValidator : AbstractValidator<CreateLeagueRequest>
    {
        public CreateLeagueRequestValidator()
        {
            RuleFor(x => x.Title)
                .Must(x => !string.IsNullOrWhiteSpace(x) && x.Length <= Constants.TitleLeagueMaxLength)
                .WithMessage($"League must have a non-empty title no more than {Constants.TitleLeagueMaxLength} characters");
        }
    }
}