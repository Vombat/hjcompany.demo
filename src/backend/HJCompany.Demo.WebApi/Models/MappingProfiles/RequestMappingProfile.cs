﻿using AutoMapper;
using HJCompany.Demo.Domain.Common.Extensions;
using HJCompany.Demo.Domain.Contracts.Models.League;
using HJCompany.Demo.Domain.Contracts.Models.Match;
using HJCompany.Demo.Domain.Contracts.Models.Team;
using HJCompany.Demo.WebApi.Models.Request.League;
using HJCompany.Demo.WebApi.Models.Request.Match;
using HJCompany.Demo.WebApi.Models.Request.Team;

namespace HJCompany.Demo.WebApi.Models.MappingProfiles
{
    public class RequestMappingProfile : Profile
    {
        public RequestMappingProfile()
        {
            CreateMap<CreateTeamRequest, CreateTeamModel>();
            CreateMap<UpdateTeamRequest, UpdateTeamModel>();
            CreateMap<CreateTeamsParticipantRequest, CreateTeamsParticipantModel>();
            CreateMap<UpdateTeamsParticipantRequest, UpdateTeamsParticipantModel>();
            CreateMap<SearchTeamsRequest, SearchTeamsFilter>();

            CreateMap<CreateLeagueRequest, CreateLeagueModel>();
            CreateMap<UpdateLeagueRequest, UpdateLeagueModel>();
            CreateMap<SearchLeaguesRequest, SearchLeaguesFilter>();


            CreateMap<CreateMatchRequest, CreateMatchModel>()
                .ForMember(src => src.Date, opts => opts.MapFrom(src => src.Date.ToDate()));
            CreateMap<UpdateMatchRequest, UpdateMatchModel>()
                .ForMember(src => src.Date, opts => opts.MapFrom(src => src.Date.ToDate()));
            CreateMap<SearchMatchesRequest, SearchMatchesFilter>();
        }
    }
}