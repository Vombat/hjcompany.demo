﻿using AutoMapper;
using HJCompany.Demo.Domain.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.League;
using HJCompany.Demo.Domain.Contracts.Models.Match;
using HJCompany.Demo.Domain.Contracts.Models.Team;
using HJCompany.Demo.WebApi.Models.Response;
using HJCompany.Demo.WebApi.Models.Response.League;
using HJCompany.Demo.WebApi.Models.Response.Match;
using HJCompany.Demo.WebApi.Models.Response.Team;

namespace HJCompany.Demo.WebApi.Models.MappingProfiles
{
    public class ResponseMappingProfile : Profile
    {
        public ResponseMappingProfile()
        {
            CreateMap<ReadTeamModel, TeamResponse>()
                .ForMember(d => d.Participants, opts => opts.MapFrom(src => src.Participants));
            CreateMap<ReadTeamsParticipantModel, TeamsParticipantResponse>();
            CreateMap<ReadLeagueModel, LeagueResponse>();
            CreateMap<ReadMatchModel, MatchResponse>()
                .ForMember(d => d.Teams, opts => opts.MapFrom(src => src.Teams));
            CreateMap(typeof(PaginationReadModel<>), typeof(PaginationResponse<>));
        }
    }
}