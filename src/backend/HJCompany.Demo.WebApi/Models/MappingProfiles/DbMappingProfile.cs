﻿using AutoMapper;
using HJCompany.Demo.DataAccess.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.League;
using HJCompany.Demo.Domain.Contracts.Models.Match;
using HJCompany.Demo.Domain.Contracts.Models.Team;

namespace HJCompany.Demo.WebApi.Models.MappingProfiles
{
    public class DbMappingProfile : Profile
    {
        public DbMappingProfile()
        {
            // Db -> Model
            CreateMap<DbTeam, ReadTeamModel>()
                .ForMember(d => d.Participants, opts => opts.MapFrom(src => src.Participants));

            CreateMap<DbTeamsParticipant, ReadTeamsParticipantModel>();

            CreateMap<DbLeague, ReadLeagueModel>();

            CreateMap<DbMatch, ReadMatchModel>();

            // Model -> Db
            CreateMap<CreateTeamModel, DbTeam>();

            CreateMap<UpdateTeamsParticipantModel, DbTeamsParticipant>();

            CreateMap<CreateTeamsParticipantModel, DbTeamsParticipant>();

            CreateMap<CreateLeagueModel, DbLeague>();

            CreateMap<CreateMatchModel, DbMatch>();
        }
    }
}