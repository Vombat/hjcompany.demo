﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HJCompany.Demo.WebApi.Infrastructure
{
    public static class Constants
    {
        public const string DefaultMimeType = "application/json";

        public const int TitleLeagueMaxLength = 200;
        public const int TitleTeamMaxLength = 200;

        public const int FirstNameMaxLength = 100;
        public const int LastNameMaxLength = 150;

        public const int TextFilterMinLength = 3;
        public const int MinOffset = 0;
        public const int MinLimit = 0;

        public static readonly Dictionary<Type, int> ExceptionMap = new Dictionary<Type, int>
        {
            {typeof(ValidationException), StatusCodes.Status400BadRequest},
            {typeof(EntryPointNotFoundException), StatusCodes.Status404NotFound}
        };
    }
}