﻿using AutoMapper;
using HJCompany.Demo.Domain.Contracts.Models.League;
using HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions;
using HJCompany.Demo.WebApi.Infrastructure;
using HJCompany.Demo.WebApi.Models.Request;
using HJCompany.Demo.WebApi.Models.Request.League;
using HJCompany.Demo.WebApi.Models.Response;
using HJCompany.Demo.WebApi.Models.Response.League;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace HJCompany.Demo.WebApi.Controllers
{
    [Produces(Constants.DefaultMimeType)]
    [Route("api/leagues")]
    [ApiController]
    public class LeagueController : ControllerBase
    {
        private const int OK = (int)HttpStatusCode.OK;
        private readonly IMapper _mapper;
        private readonly ILeagueRepository _leagueRepository;

        public LeagueController(ILeagueRepository leagueRepository, IMapper mapper)
        {
            _leagueRepository = leagueRepository;
            _mapper = mapper;
        }

        /// <summary>
        ///     Поиск лиги
        /// </summary>
        [HttpPost]
        [Route("search")]
        [SwaggerResponse(OK, Type = typeof(IEnumerable<LeagueResponse>))]
        public async Task<IActionResult> SearchLeagues(SearchLeaguesRequest request)
        {
            var paginationResult = await _leagueRepository.SearchLeaguesAsync(_mapper.Map<SearchLeaguesFilter>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<PaginationResponse<LeagueResponse>>(paginationResult));
        }

        /// <summary>
        ///     Получение лиги по Id
        /// </summary>
        [HttpGet]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(LeagueResponse))]
        public async Task<IActionResult> GetLeague([FromRoute] ByGuidRequest model)
        {
            var league = await _leagueRepository.GetLeagueAsync(model.Id, HttpContext.RequestAborted);
            return Ok(_mapper.Map<LeagueResponse>(league));
        }

        /// <summary>
        ///     Создание лиги
        /// </summary>
        [HttpPost]
        [SwaggerResponse(OK, Type = typeof(LeagueResponse))]
        public async Task<IActionResult> CreateLeague(CreateLeagueRequest request)
        {
            var league = await _leagueRepository.CreateLeagueAsync(_mapper.Map<CreateLeagueModel>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<LeagueResponse>(league));
        }

        /// <summary>
        ///     Обновление лиги
        /// </summary>
        [HttpPut]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(LeagueResponse))]
        public async Task<IActionResult> UpdateLeague([FromRoute] ByGuidRequest idModel, UpdateLeagueRequest request)
        {
            var league = await _leagueRepository.UpdateLeagueAsync(idModel.Id, _mapper.Map<UpdateLeagueModel>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<LeagueResponse>(league));
        }

        /// <summary>
        ///     Удаление лиги
        /// </summary>
        [HttpDelete]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(LeagueResponse))]
        public async Task<IActionResult> DeleteLeague([FromRoute] ByGuidRequest model)
        {
            await _leagueRepository.DeleteLeagueAsync(model.Id, HttpContext.RequestAborted);
            return Ok();
        }
    }
}