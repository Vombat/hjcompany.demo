﻿using AutoMapper;
using HJCompany.Demo.Domain.Contracts.Models.Team;
using HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions;
using HJCompany.Demo.WebApi.Infrastructure;
using HJCompany.Demo.WebApi.Models.Request;
using HJCompany.Demo.WebApi.Models.Request.Team;
using HJCompany.Demo.WebApi.Models.Response;
using HJCompany.Demo.WebApi.Models.Response.Team;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace HJCompany.Demo.WebApi.Controllers
{
    [Produces(Constants.DefaultMimeType)]
    [Route("api/teams")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private const int OK = (int)HttpStatusCode.OK;
        private readonly IMapper _mapper;
        private readonly ITeamRepository _teamRepository;

        public TeamController(ITeamRepository teamRepository, IMapper mapper)
        {
            _teamRepository = teamRepository;
            _mapper = mapper;
        }

        /// <summary>
        ///     Поиск команды
        /// </summary>
        [HttpPost]
        [Route("search")]
        [SwaggerResponse(OK, Type = typeof(IEnumerable<TeamResponse>))]
        public async Task<IActionResult> SearchTeams(SearchTeamsRequest request)
        {
            var paginationResult = await _teamRepository.SearchTeamsAsync(_mapper.Map<SearchTeamsFilter>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<PaginationResponse<TeamResponse>>(paginationResult));
        }

        /// <summary>
        ///     Получение команды по Id
        /// </summary>
        [HttpGet]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(TeamResponse))]
        public async Task<IActionResult> GetTeam([FromRoute] ByGuidRequest model)
        {
            var team = await _teamRepository.GetTeamAsync(model.Id, HttpContext.RequestAborted);
            return Ok(_mapper.Map<TeamResponse>(team));
        }

        /// <summary>
        ///     Создание команды
        /// </summary>
        [HttpPost]
        [SwaggerResponse(OK, Type = typeof(TeamResponse))]
        public async Task<IActionResult> CreateTeam(CreateTeamRequest request)
        {
            var team = await _teamRepository.CreateTeamAsync(_mapper.Map<CreateTeamModel>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<TeamResponse>(team));
        }

        /// <summary>
        ///     Обновление команды
        /// </summary>
        [HttpPut]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(TeamResponse))]
        public async Task<IActionResult> UpdateTeam([FromRoute] ByGuidRequest idModel, UpdateTeamRequest request)
        {
            var team = await _teamRepository.UpdateTeamAsync(idModel.Id, _mapper.Map<UpdateTeamModel>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<TeamResponse>(team));
        }

        /// <summary>
        ///     Удаление команды
        /// </summary>
        [HttpDelete]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(TeamResponse))]
        public async Task<IActionResult> DeleteTeam([FromRoute] ByGuidRequest model)
        {
            await _teamRepository.DeleteTeamAsync(model.Id, HttpContext.RequestAborted);
            return Ok();
        }

        /// <summary>
        ///     Добавление участника команды
        /// </summary>
        [HttpPost]
        [Route("{id:guid}/participants")]
        [SwaggerResponse(OK, Type = typeof(TeamResponse))]
        public async Task<IActionResult> AddTeamsParticipant([FromRoute] ByGuidRequest idModel, [FromBody] AddTeamsParticipantRequest model)
        {
            var teamsParticipant = await _teamRepository.AddTeamsParticipantAsync(idModel.Id, _mapper.Map<CreateTeamsParticipantModel>(model), HttpContext.RequestAborted);
            return Ok(_mapper.Map<TeamsParticipantResponse>(teamsParticipant));
        }

        /// <summary>
        ///     Обновление участника команды
        /// </summary>
        [HttpPut]
        [Route("{teamId:guid}/participants/{participantId:guid}")]
        [SwaggerResponse(OK, Type = typeof(TeamResponse))]
        public async Task<IActionResult> UpdateTeamsParticipant([FromRoute] ByTeamIdAndParticipantRequest idModel,
            [FromBody] UpdateTeamsParticipantRequest model)
        {
            var teamsParticipant = await _teamRepository.UpdateTeamsParticipantAsync(idModel.TeamId, idModel.ParticipantId, _mapper.Map<UpdateTeamsParticipantModel>(model), HttpContext.RequestAborted);
            return Ok(_mapper.Map<TeamsParticipantResponse>(teamsParticipant));
        }

        /// <summary>
        ///     Удаление участника команды
        /// </summary>
        [HttpDelete]
        [Route("{teamId:guid}/participants/{participantId:guid}")]
        [SwaggerResponse(OK, Type = typeof(TeamResponse))]
        public async Task<IActionResult> DeleteTeamsParticipant([FromRoute] ByTeamIdAndParticipantRequest model)
        {
            await _teamRepository.DeleteTeamsParticipantAsync(model.TeamId, model.ParticipantId, HttpContext.RequestAborted);
            return Ok();
        }
    }
}