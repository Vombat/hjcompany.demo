﻿using AutoMapper;
using HJCompany.Demo.Domain.Contracts.Models.Match;
using HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions;
using HJCompany.Demo.WebApi.Infrastructure;
using HJCompany.Demo.WebApi.Models.Request;
using HJCompany.Demo.WebApi.Models.Request.Match;
using HJCompany.Demo.WebApi.Models.Request.Team;
using HJCompany.Demo.WebApi.Models.Response;
using HJCompany.Demo.WebApi.Models.Response.Match;
using HJCompany.Demo.WebApi.Models.Response.Team;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace HJCompany.Demo.WebApi.Controllers
{
    [Produces(Constants.DefaultMimeType)]
    [Route("api/matches")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private const int OK = (int)HttpStatusCode.OK;
        private readonly IMapper _mapper;
        private readonly IMatchRepository _matchRepository;

        public MatchController(IMatchRepository matchRepository, IMapper mapper)
        {
            _matchRepository = matchRepository;
            _mapper = mapper;
        }

        /// <summary>
        ///     Поиск матча
        /// </summary>
        [HttpPost]
        [Route("search")]
        [SwaggerResponse(OK, Type = typeof(IEnumerable<MatchResponse>))]
        public async Task<IActionResult> SearchMatches(SearchMatchesRequest request)
        {
            var paginationResult = await _matchRepository.SearchMatchesAsync(_mapper.Map<SearchMatchesFilter>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<PaginationResponse<MatchResponse>>(paginationResult));
        }

        /// <summary>
        ///     Получение матча по Id
        /// </summary>
        [HttpGet]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(MatchResponse))]
        public async Task<IActionResult> GetMatch([FromRoute] ByGuidRequest model)
        {
            var match = await _matchRepository.GetMatchAsync(model.Id, HttpContext.RequestAborted);
            return Ok(_mapper.Map<MatchResponse>(match));
        }

        /// <summary>
        ///     Создание матча
        /// </summary>
        [HttpPost]
        [SwaggerResponse(OK, Type = typeof(MatchResponse))]
        public async Task<IActionResult> CreateMatch(CreateMatchRequest request)
        {
            var match = await _matchRepository.CreateMatchAsync(_mapper.Map<CreateMatchModel>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<MatchResponse>(match));
        }

        /// <summary>
        ///     Обновление матча
        /// </summary>
        [HttpPut]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(MatchResponse))]
        public async Task<IActionResult> UpdateMatch([FromRoute] ByGuidRequest idModel, UpdateMatchRequest request)
        {
            var match = await _matchRepository.UpdateMatchAsync(idModel.Id, _mapper.Map<UpdateMatchModel>(request), HttpContext.RequestAborted);
            return Ok(_mapper.Map<MatchResponse>(match));
        }

        /// <summary>
        ///     Удаление матча
        /// </summary>
        [HttpDelete]
        [Route("{id:guid}")]
        [SwaggerResponse(OK, Type = typeof(MatchResponse))]
        public async Task<IActionResult> DeleteMatch([FromRoute] ByGuidRequest model)
        {
            await _matchRepository.DeleteMatchAsync(model.Id, HttpContext.RequestAborted);
            return Ok();
        }

        /// <summary>
        ///     Добавление команды на матч
        /// </summary>
        [HttpPost]
        [Route("{id:guid}/teams")]
        [SwaggerResponse(OK, Type = typeof(MatchResponse))]
        public async Task<IActionResult> AddMatchesTeam([FromRoute] ByGuidRequest idModel, [FromBody] CreateTeamRequest model)
        {
            var matchesTeam = await _matchRepository.AddMatchesTeamAsync(idModel.Id, _mapper.Map<CreateMatchesTeamModel>(model), HttpContext.RequestAborted);
            return Ok(_mapper.Map<TeamResponse>(matchesTeam));
        }

        /// <summary>
        ///     Обновление команды матча
        /// </summary>
        [HttpPut]
        [Route("{matchId:guid}/teams/{teamId:guid}")]
        [SwaggerResponse(OK, Type = typeof(MatchResponse))]
        public async Task<IActionResult> UpdateMatchesTeam([FromRoute] ByMatchIdAndTeamRequest idModel, [FromBody] UpdateTeamsParticipantRequest model)
        {
            var matchesTeam = await _matchRepository.UpdateMatchesTeamAsync(idModel.MatchId, idModel.TeamId, _mapper.Map<UpdateMatchesTeamModel>(model), HttpContext.RequestAborted);
            return Ok(_mapper.Map<TeamResponse>(matchesTeam));
        }

        /// <summary>
        ///     Удаление команды с матча
        /// </summary>
        [HttpDelete]
        [Route("{matchId:guid}/teams/{teamId:guid}")]
        [SwaggerResponse(OK, Type = typeof(MatchResponse))]
        public async Task<IActionResult> DeleteMatchesTeam([FromRoute] ByMatchIdAndTeamRequest model)
        {
            await _matchRepository.DeleteMatchesTeamAsync(model.MatchId, model.TeamId, HttpContext.RequestAborted);
            return Ok();
        }
    }
}