using Autofac;
using AutoMapper;
using FluentValidation.AspNetCore;
using HJCompany.Demo.DataAccess.EntityFramework.Extensions;
using HJCompany.Demo.Domain.Core.Extensions;
using HJCompany.Demo.WebApi.AppConfiguration;
using HJCompany.Demo.WebApi.Infrastructure.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Serilog;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace HJCompany.Demo.WebApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _hostEnvironment;

        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            _configuration = configuration;
            _hostEnvironment = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor()
                .AddCors()
                .AddControllers()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.Converters.Add(new StringEnumConverter(new CamelCaseNamingStrategy())))
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    options.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                });

            services.AddDatabase(_configuration);

            services.AddSwagger(_hostEnvironment, "WebApi")
                .AddHealthChecks();

            services.AddAutoMapper(cfg => cfg.DisableConstructorMapping(), Assembly.GetExecutingAssembly());
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            // This will all go in the ROOT CONTAINER and is NOT TENANT SPECIFIC.
            builder.AddCore();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<HttpStatusCodeExceptionMiddleware>();
            app.UseHsts();
            app.UseSerilogRequestLogging();
            app.UseRouting();

            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowAnyOrigin());

            // use default localization, don't trust the hosting env!
            app.UseRequestLocalization(
                new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture(CultureInfo.InvariantCulture, CultureInfo.InvariantCulture),
                    SupportedCultures = new List<CultureInfo> { CultureInfo.InvariantCulture },
                    SupportedUICultures = new List<CultureInfo> { CultureInfo.InvariantCulture },
                    FallBackToParentCultures = true,
                    FallBackToParentUICultures = true,
                    RequestCultureProviders = new List<IRequestCultureProvider>()
                });

            app.UseSwaggerAndConfigure(env, "WebApi");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });

            app.ApplyDbMigrations();
        }
    }
}