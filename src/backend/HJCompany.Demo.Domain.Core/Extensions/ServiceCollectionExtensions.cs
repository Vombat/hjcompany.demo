﻿using Autofac;
using HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions;
using HJCompany.Demo.Domain.Core.Repositories;

namespace HJCompany.Demo.Domain.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static ContainerBuilder AddCore(this ContainerBuilder builder)
        {
            builder.RegisterType<LeagueRepository>().As<ILeagueRepository>().InstancePerLifetimeScope();
            builder.RegisterType<TeamRepository>().As<ITeamRepository>().InstancePerLifetimeScope();
            builder.RegisterType<MatchRepository>().As<IMatchRepository>().InstancePerLifetimeScope();
            return builder;
        }
    }
}