﻿using AutoMapper;
using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using HJCompany.Demo.DataAccess.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.League;
using HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions;
using HJCompany.Demo.Domain.Core.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HJCompany.Demo.Domain.Core.Repositories
{
    public class LeagueRepository : ILeagueRepository
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public LeagueRepository(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<ReadLeagueModel> GetLeagueAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbLeague = await GetEntityAsync<DbLeague>(id, cancellationToken);
            return _mapper.Map<ReadLeagueModel>(dbLeague);
        }

        public async Task<PaginationReadModel<ReadLeagueModel>> SearchLeaguesAsync(SearchLeaguesFilter filter, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.Get<DbLeague>();

            if (!string.IsNullOrWhiteSpace(filter.TextFilter))
            {
                query = query.Where(x => EF.Functions.Like(x.Title, $"{filter.TextFilter}%"));
            }

            var count = await query.CountAsync(cancellationToken);

            //var leagues = await query
            //    .Skip(filter.Offset)
            //    .Take(filter.Limit)
            //    .Select(o => _mapper.Map<ReadLeagueModel>(o))
            //    .ToListAsync(cancellationToken);

            var leagues = (await query
                .Skip(filter.Offset)
                .Take(filter.Limit)
                .AsNoTracking()
                .ToListAsync(cancellationToken))
               .Select(x => _mapper.Map<ReadLeagueModel>(x));
               


            return new PaginationReadModel<ReadLeagueModel>(leagues, count);
        }

        public async Task<ReadLeagueModel> CreateLeagueAsync(CreateLeagueModel model, CancellationToken cancellationToken)
        {
            var dbLeague = _mapper.Map<DbLeague>(model);
            dbLeague = _unitOfWork.Create(dbLeague);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadLeagueModel>(dbLeague);
        }

        public async Task<ReadLeagueModel> UpdateLeagueAsync(Guid id, UpdateLeagueModel model, CancellationToken cancellationToken)
        {
            var dbLeague = await GetEntityAsync<DbLeague>(id, cancellationToken);
            dbLeague.Update(model.Title);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadLeagueModel>(dbLeague);
        }

        public async Task DeleteLeagueAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbLeague = await GetEntityAsync<DbLeague>(id, cancellationToken);
            _unitOfWork.Delete(dbLeague);
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        private async Task<T> GetEntityAsync<T>(Guid id, CancellationToken cancellationToken) where T : BaseGuidEntity
        {
            var dbEntity = await _unitOfWork.Get<T>().FirstOrDefaultAsync(o => o.Id == id, cancellationToken);
            if (dbEntity == null)
            {
                throw EntityNotFoundException.CreateFromEntity<T, Guid>(id);
            }

            return dbEntity;
        }
    }
}
