﻿using AutoMapper;
using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using HJCompany.Demo.DataAccess.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.Team;
using HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions;
using HJCompany.Demo.Domain.Core.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HJCompany.Demo.Domain.Core.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public TeamRepository(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<ReadTeamModel> GetTeamAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbTeam = await GetEntityAsync<DbTeam>(id, cancellationToken);
            return _mapper.Map<ReadTeamModel>(dbTeam);
        }

        public async Task<PaginationReadModel<ReadTeamModel>> SearchTeamsAsync(SearchTeamsFilter filter, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.Get<DbTeam>();

            if (!string.IsNullOrWhiteSpace(filter.TextFilter))
            {
                query = query.Where(x => EF.Functions.Like(x.Title, $"{filter.TextFilter}%"));
            }

            var count = await query.CountAsync(cancellationToken);

            //var teams = await query
            //    .Skip(filter.Offset)
            //    .Take(filter.Limit)
            //    .Select(o => _mapper.Map<ReadTeamModel>(o))
            //    .ToListAsync(cancellationToken);

            var teams = (await query
                .Skip(filter.Offset)
                .Take(filter.Limit)                
                .ToListAsync(cancellationToken)).Select(o => _mapper.Map<ReadTeamModel>(o));

            return new PaginationReadModel<ReadTeamModel>(teams, count);
        }

        public async Task<ReadTeamModel> CreateTeamAsync(CreateTeamModel model, CancellationToken cancellationToken)
        {
            var dbTeam = _mapper.Map<DbTeam>(model);
            dbTeam = _unitOfWork.Create(dbTeam);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadTeamModel>(dbTeam);
        }

        public async Task<ReadTeamModel> UpdateTeamAsync(Guid id, UpdateTeamModel model, CancellationToken cancellationToken)
        {
            var dbTeam = await GetEntityAsync<DbTeam>(id, cancellationToken);
            dbTeam.Update(model.MatchId, model.Title);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadTeamModel>(dbTeam);
        }

        public async Task DeleteTeamAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbTeam = await GetEntityAsync<DbTeam>(id, cancellationToken);
            _unitOfWork.Delete(dbTeam);
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        public async Task<ReadTeamsParticipantModel> AddTeamsParticipantAsync(Guid teamId, CreateTeamsParticipantModel model, CancellationToken cancellationToken)
        {
            var dbTeamsParticipant = _mapper.Map<DbTeamsParticipant>(model);
            dbTeamsParticipant.TeamId = teamId;
            dbTeamsParticipant = _unitOfWork.Create(dbTeamsParticipant);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadTeamsParticipantModel>(dbTeamsParticipant);
        }

        public async Task<ReadTeamsParticipantModel> UpdateTeamsParticipantAsync(Guid teamId, Guid participantId, UpdateTeamsParticipantModel model, CancellationToken cancellationToken)
        {
            var dbTeamsParticipant = await GetEntityAsync<DbTeamsParticipant>(participantId, cancellationToken);
            dbTeamsParticipant.Update(model.FirstName, model.LastName);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadTeamsParticipantModel>(dbTeamsParticipant);
        }

        public async Task DeleteTeamsParticipantAsync(Guid teamId, Guid participantId, CancellationToken cancellationToken)
        {
            var dbTeam = await GetEntityAsync<DbTeam>(teamId, cancellationToken);
            var dbTeamsParticipant = await GetEntityAsync<DbTeamsParticipant>(participantId, cancellationToken);
            if (dbTeam.Participants.Count == 1)
            {
                throw new InvalidOperationException("Can't delete the only team's participant");
            }
            _unitOfWork.Delete(dbTeamsParticipant);
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        private async Task<T> GetEntityAsync<T>(Guid id, CancellationToken cancellationToken) where T : BaseGuidEntity
        {
            var dbEntity = await _unitOfWork.Get<T>().FirstOrDefaultAsync(o => o.Id == id, cancellationToken);
            if (dbEntity == null)
            {
                throw EntityNotFoundException.CreateFromEntity<T, Guid>(id);
            }

            return dbEntity;
        }
    }
}