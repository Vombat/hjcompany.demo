﻿using AutoMapper;
using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using HJCompany.Demo.DataAccess.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models;
using HJCompany.Demo.Domain.Contracts.Models.Match;
using HJCompany.Demo.Domain.Contracts.RepositoriesAbstractions;
using HJCompany.Demo.Domain.Core.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HJCompany.Demo.Domain.Core.Repositories
{
    public class MatchRepository : IMatchRepository
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public MatchRepository(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<ReadMatchModel> GetMatchAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbMatch = await GetEntityAsync<DbMatch>(id, cancellationToken);
            return _mapper.Map<ReadMatchModel>(dbMatch);
        }

        public async Task<PaginationReadModel<ReadMatchModel>> SearchMatchesAsync(SearchMatchesFilter filter, CancellationToken cancellationToken)
        {
            var query = _unitOfWork.Get<DbMatch>();

            if (filter.Date.HasValue)
            {               
                query = query.Where(o => o.Date == filter.Date.Value);
            }

            if (filter.LeagueId.HasValue)
            {
                query = query.Where(o => o.LeagueId == filter.LeagueId.Value);
            }           

            var count = await query.CountAsync(cancellationToken);

            //var matches = await query
            //    .Skip(filter.Offset)
            //    .Take(filter.Limit)
            //    .Select(o => _mapper.Map<ReadMatchModel>(o))
            //    .ToListAsync(cancellationToken);

            var matches = (await query
                .Skip(filter.Offset)
                .Take(filter.Limit)                
                .ToListAsync(cancellationToken)).Select(o => _mapper.Map<ReadMatchModel>(o));

            return new PaginationReadModel<ReadMatchModel>(matches, count);
        }

        public async Task<ReadMatchModel> CreateMatchAsync(CreateMatchModel model, CancellationToken cancellationToken)
        {
            var dbMatch = _mapper.Map<DbMatch>(model);
            dbMatch = _unitOfWork.Create(dbMatch);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadMatchModel>(dbMatch);
        }

        public async Task<ReadMatchModel> UpdateMatchAsync(Guid id, UpdateMatchModel model, CancellationToken cancellationToken)
        {
            var dbMatch = await GetEntityAsync<DbMatch>(id, cancellationToken);
            dbMatch.Update(model.LeagueId, model.Date);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadMatchModel>(dbMatch);
        }

        public async Task DeleteMatchAsync(Guid id, CancellationToken cancellationToken)
        {
            var dbMatch = await GetEntityAsync<DbMatch>(id, cancellationToken);
            _unitOfWork.Delete(dbMatch);
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        public async Task<ReadMatchesTeamModel> AddMatchesTeamAsync(Guid matchId, CreateMatchesTeamModel model, CancellationToken cancellationToken)
        {
            var dbMatchesTeam = _mapper.Map<DbTeam>(model);
            dbMatchesTeam.MatchId = matchId;
            dbMatchesTeam = _unitOfWork.Create(dbMatchesTeam);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadMatchesTeamModel>(dbMatchesTeam);
        }

        public async Task<ReadMatchesTeamModel> UpdateMatchesTeamAsync(Guid matchId, Guid teamId, UpdateMatchesTeamModel model, CancellationToken cancellationToken)
        {           
            var dbMatch = await GetEntityAsync<DbMatch>(matchId, cancellationToken);
            var dbTeam = await GetEntityAsync<DbTeam>(teamId, cancellationToken);
            if (dbMatch.Id != dbTeam.MatchId)
            {
                throw new InvalidOperationException($"A team with name {dbTeam.Title} is not entered for this match");
            }
            dbTeam.Update(model.MatchesTeam.MatchId, model.MatchesTeam.Title);
            await _unitOfWork.CommitAsync(cancellationToken);
            return _mapper.Map<ReadMatchesTeamModel>(dbTeam);
        }

        public async Task DeleteMatchesTeamAsync(Guid matchId, Guid teamId, CancellationToken cancellationToken)
        {
            var dbMatch = await GetEntityAsync<DbMatch>(matchId, cancellationToken);
            var dbMatchesTeam = await GetEntityAsync<DbTeam>(teamId, cancellationToken);
            if (dbMatch.Id != dbMatchesTeam.MatchId)
            {
                throw new InvalidOperationException($"A team with name {dbMatchesTeam.Title} is not entered for this match");
            }
            _unitOfWork.Delete(dbMatchesTeam);
            await _unitOfWork.CommitAsync(cancellationToken);
        }

        private async Task<T> GetEntityAsync<T>(Guid id, CancellationToken cancellationToken) where T : BaseGuidEntity
        {
            var dbEntity = await _unitOfWork.Get<T>().FirstOrDefaultAsync(o => o.Id == id, cancellationToken);
            if (dbEntity == null)
            {
                throw EntityNotFoundException.CreateFromEntity<T, Guid>(id);
            }

            return dbEntity;
        }
    }
}