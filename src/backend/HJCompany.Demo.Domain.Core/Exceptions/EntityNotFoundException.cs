﻿using System;

namespace HJCompany.Demo.Domain.Core.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string message) : base(message) { }

        public EntityNotFoundException(string message, Exception exception) : base(message, exception) { }

        public static EntityNotFoundException CreateFromEntity<TEntity, TKey>(TKey id)
        {
            return new EntityNotFoundException($"Entity {typeof(TEntity).Name} with id {id} not found");
        }
    }
}