﻿using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace HJCompany.Demo.DataAccess.Contracts.Models
{
    public class DbLeague : BaseGuidEntity
    {
        protected DbLeague() { }

        public DbLeague(string title)
        {            
            Title = title;
        }
        
        public string Title { get; protected set; }

        public void Update(string title)
        {
            Title = title ?? Title;
        }
    }
}