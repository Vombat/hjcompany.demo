﻿using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.DataAccess.Contracts.Models
{
    public class DbMatch : BaseGuidEntity
    {
        protected DbMatch() { }

        public DbMatch(Guid leagueId, DateTime date)
        {
            LeagueId = leagueId;
            Date = date;
        }

        public Guid LeagueId { get; protected set; }
        public DateTime Date { get; protected set; }
        public virtual ICollection<DbTeam> Teams { get; protected set; } = new List<DbTeam>();

        public void Update(Guid? leagueId, DateTime? date)
        {
            LeagueId = leagueId ?? LeagueId;
            Date = date ?? Date;
        }
    }
}