﻿using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using System;
using System.Collections.Generic;

namespace HJCompany.Demo.DataAccess.Contracts.Models
{
    public class DbTeam : BaseGuidEntity
    {
        protected DbTeam() { }

        public DbTeam(Guid? matchId, string title)
        {
            MatchId = matchId;
            Title = title;
        }

        public Guid? MatchId { get; set; }
        public virtual DbMatch Match { get; protected set; }

        public string Title { get; protected set; }
        public virtual ICollection<DbTeamsParticipant> Participants { get; protected set; } = new List<DbTeamsParticipant>();

        public void Update(Guid? matchId, string title)
        {
            MatchId = matchId;
            Title = title ?? Title;
        }
    }
}