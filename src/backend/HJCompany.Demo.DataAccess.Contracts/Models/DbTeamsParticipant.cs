﻿using HJCompany.Demo.DataAccess.Contracts.Abstractions;
using System;

namespace HJCompany.Demo.DataAccess.Contracts.Models
{
    public class DbTeamsParticipant : BaseGuidEntity
    {
        protected DbTeamsParticipant() { }

        public DbTeamsParticipant(Guid teamId, string firstName, string lastName)
        {
            TeamId = teamId;
            FirstName = firstName;
            LastName = lastName;
        }

        public Guid TeamId { get; set; }
        public virtual DbTeam Team { get; protected set; }

        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }

        public void Update(string firstName, string lastName)
        {
            FirstName = firstName ?? FirstName;
            LastName = lastName ?? LastName;
        }
    }
}