﻿using System;

namespace HJCompany.Demo.DataAccess.Contracts.Abstractions
{
    public interface IUpdateDateTrackedEntity
    {
        DateTimeOffset DateUpdated { get; set; }
    }
}