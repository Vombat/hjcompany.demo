﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HJCompany.Demo.DataAccess.Contracts.Abstractions
{
    public class BaseEntity : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
    }

    public class BaseGuidEntity : IBaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj.GetType() == GetType() && Equals((BaseGuidEntity)obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        private bool Equals(BaseGuidEntity other)
        {
            return Id == other.Id;
        }
    }
}