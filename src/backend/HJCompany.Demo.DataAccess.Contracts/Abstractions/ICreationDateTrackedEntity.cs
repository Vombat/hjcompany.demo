﻿using System;

namespace HJCompany.Demo.DataAccess.Contracts.Abstractions
{
    public interface ICreationDateTrackedEntity
    {
        DateTimeOffset DateCreated { get; set; }
    }
}