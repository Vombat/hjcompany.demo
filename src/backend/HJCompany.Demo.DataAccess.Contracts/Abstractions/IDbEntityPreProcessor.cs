﻿using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace HJCompany.Demo.DataAccess.Contracts.Abstractions
{
    public interface IDbEntityPreProcessor
    {
        void Process(ChangeTracker changeTracker);
    }
}